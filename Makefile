#
#
#               Ezanvakti 5.7 Makefile
#
#


SHELL = /bin/bash
surum = $(shell sed -n 's:SURUM=::p' src/usr/bin/ezanvakti)
derleme = $(shell git log -1 --pretty=format:'%ad' --abbrev-commit --date=short 2>/dev/null | tr -d -- '-')


ifeq "$(derleme)" ""
	derleme = bilinmeyen
endif


all:
		@echo "Nothing to make, use 'make install' to perform an installation."

install:
		install  -vd $(DESTDIR)/usr/share/sounds

		install -vDm644 src/etc/bash_completion.d/ezanvakti $(DESTDIR)/etc/bash_completion.d/ezanvakti
		install -vDm644 src/etc/xdg/autostart/ezanvakti.desktop $(DESTDIR)/etc/xdg/autostart/ezanvakti.desktop
		install -vDm755 src/usr/bin/ezanvakti $(DESTDIR)/usr/bin/ezanvakti
		
		@cp -vR src/usr/share/ezanvakti $(DESTDIR)/usr/share/ezanvakti
		install -vDm755 src/usr/share/applications/ezanvakti.desktop $(DESTDIR)/usr/share/applications/ezanvakti.desktop
		install -vDm644 src/usr/share/man/man1/ezanvakti.1 $(DESTDIR)/usr/share/man/man1/ezanvakti.1
		@cp -vR src/usr/share/sounds/ezanvakti $(DESTDIR)/usr/share/sounds/ezanvakti
		
		@chmod -v 755 $(DESTDIR)/usr/share/ezanvakti/bilesenler/{ezanveri_guncelle,renk_ogren}


uninstall:
		@rm -f  $(DESTDIR)/etc/bash_completion.d/ezanvakti
		@rm -f  $(DESTDIR)/etc/xdg/autostart/ezanvakti.desktop
		@rm -f  $(DESTDIR)/usr/bin/ezanvakti
		@rm -rf $(DESTDIR)/usr/share/ezanvakti
		@rm -f  $(DESTDIR)/usr/share/applications/ezanvakti.desktop
		@rm -f  $(DESTDIR)/usr/share/man/man1/ezanvakti.1
		@rm -rf $(DESTDIR)/usr/share/sounds/ezanvakti
		@echo "ezanvakti başarıyla sisteminizden kaldırıldı.."
		
dist:
		@echo "Kaynak kod paketi oluşturuluyor. Lütfen bekleyiniz..."
		@git archive master | xz > ezanvakti-$(surum).$(derleme).tar.xz
		@echo "İşlem tamamlandı. ----------> ezanvakti-$(surum).$(derleme).tar.xz"

.PHONY: all install uninstall dist