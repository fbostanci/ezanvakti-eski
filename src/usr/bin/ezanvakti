#!/bin/bash
#
#                 Ezanvakti 5.7  -  GNU/Linux Ezan Vakti Bildirici
#
##
##          Copyright (c) 2010-2012  Fatih Bostancı  <faopera@gmail.com>
##
##                   https://gitorious.org/ezanvakti
##
##
##    Bu uygulama bir özgür yazılımdır: yeniden dağıtabilirsiniz ve/veya
##    Özgür Yazılım Vakfı (FSF) tarafından yayımlanan (GPL)  Genel  kamu
##    lisansı sürüm 3  veya daha yeni bir sürümünde belirtilen  şartlara
##    uymak kaydıyla, üzerinde değişiklik yapabilirsiniz.
##
##    Ayrıntılar için COPYING dosyasını okuyun.
#
#

EZANVAKTI_DIZINI="${XDG_CONFIG_HOME:-$HOME/.config}"/ezanvakti
EZANVAKTI_AYAR="${EZANVAKTI_DIZINI}"/ayarlar
VERI_DIZINI=/usr/share/ezanvakti

SURUM=5.7
DERLEME=20120505

[ ! -d "${EZANVAKTI_DIZINI}" ] && mkdir -p "${EZANVAKTI_DIZINI}"
[ ! -f "${EZANVAKTI_AYAR}"   ] && . ${VERI_DIZINI}/bilesenler/ayarlari_olustur

# Öntanımlı ayarları al.
. ${VERI_DIZINI}/veriler/ayarlar

# Kullanıcı ayarlarını al.
. "${EZANVAKTI_AYAR}" || \
  { printf "${EZANVAKTI_AYAR} dosyası okunabilir değil. Çıkılıyor...\n"; exit 1; }

EZANVERI="${EZANVERI_DIZINI}/${EZANVERI_ADI}"
MPLAYER="mplayer -really-quiet -volume $SES"

# Bileşenleri çalışma sürecine dahil ediyoruz.
. ${VERI_DIZINI}/bilesenler/guncelleyici
. ${VERI_DIZINI}/bilesenler/oynatici_duraklat
. ${VERI_DIZINI}/bilesenler/kullanim

# Renk kullan sıfır değilse renkleri sıfırla.
(( ${RENK:-RENK_KULLAN} )) && {
  RENK0=''
  RENK1=''
  RENK2=''
  RENK3=''
  RENK4=''
  RENK5=''
  RENK6=''
  RENK7=''
  RENK8=''
}

# ezanveri dosyasının kullanıldığı işlemler için olumsuz durumları denetle.
function denetle() {
  local ksatir
  # ezanlar ve iftarimsak fonksiyonları özyinelemeli çalıştığı için
  # gün değişiminde tarihi güncellemek için tarih, denetle fonksiyonu içine alındı.
  TARIH=`date +%d.%m.%Y`
  SAAT=`date +%H%M`

  [ ! -f "${EZANVERI}" ] && { # ezanveri dosyası yoksa
    (( ! GUNCELLEME_YAP )) && { # otomatik güncelleme etkinse
      guncelleme_yap # ilgili fonksiyonu çağır ve ezanveri dosyasını oluştur.
    } || { # otomatik güncelleme kapalıysa..
      echo -e "${RENK7}${RENK2}${EZANVERI}${RENK3} dosyası bulunamadı.\nÇıkılıyor...${RENK0}"
      exit 1
    }
  }

  # Bugüne ait tarih ezanveri dosyasında yoksa
  [[ -z $(grep -o $TARIH "${EZANVERI}") ]] && {
    (( ! GUNCELLEME_YAP )) && {
      guncelleme_yap
    } || {
      echo -e "${RENK7}${RENK2}${EZANVERI}${RENK3} dosyası güncel değil.\nÇıkılıyor...${RENK0}"
      exit 1
    }
  }

  # ezanveri dosyasından tarih bloğunu ayıklayıp son tarih satır no dan bugünkünü çıkardık.
  ksatir=$(gawk -v tarih=${TARIH} '/^[0-9][0-9]\.[0-9]*\.[0-9]*/ {if($1 ~ tarih) bsatir = NR}; \
    /^[0-9][0-9]\.[0-9]*\.[0-9]*/ {} END {tsatir = NR}; END {print(tsatir-bsatir)}' "${EZANVERI}")
  let ksatir++

  (( ksatir <= 7 )) && { # sonuç 7 ya da 7'den küçükse
    (( ! GUNCELLEME_YAP )) && {
      guncelleme_yap
    }

    # Betiğin mevcut oturum boyunca sadece ilk çalışmada bildirim vermesi
    # için çerez dosya denetimi ekledik. Gün değişimi durumu için (mevcut oturum devam ediyorsa)
    # ayrıca dosyaya tarih uzantısı da ekledik.
    [ ! -f /tmp/eznvrgncldntle_$(date +%d%m%y) ] && {
      notify-send "Ezanvakti $SURUM" "${EZANVERI_ADI} dosyanız \n\t$ksatir gün \nsonra güncelliğini yitirecek." \
        -i ${VERI_DIZINI}/veriler/ezanvakti.png -t $GUNCELLEME_BILDIRIM_SURESI"000" -h int:transient:1
      :> /tmp/eznvrgncldntle_$(date +%d%m%y)
    }
  }
}

# Bugünün ezan vakitlerini ayıkla ve değerleri vakit değişkenlerine ata.
function bugun() {
  export $(gawk 'BEGIN{tarih = strftime("%d.%m.%Y")} {if($1 ~ tarih) \
    {printf "sabah=%s%s\ngunes=%s%s\nogle=%s%s\nikindi=%s%s\naksam=%s%s\
    \nyatsi=%s%s\nsabah_n=0%s:%s\ngunes_n=0%s:%s\nogle_n=%s:%s\nikindi_n=%s:%s\naksam_n=%s:%s\nyatsi_n=%s:%s"\
    , $2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13}}' "${EZANVERI}")
}

# Eğer hedef süre bugün içerindeyse bu fonksiyon kullanılacak.
# Eğer hedef süre bugün içerindeyse bu fonksiyon kullanılacak.
function bekleme_suresi() {
  bekle=$(($(date -d "$1" +%s) - $(date +%s) + EZAN_OKUNMA_SURESI_FARKI))
}

#  Eğer hedef süre yarına aitse bu fonksiyon kullanılacak.
function bekleme_suresi_yarin() {
  bekle=$((86400 - $(date +%s) + $(date -d "$1" +%s) + EZAN_OKUNMA_SURESI_FARKI))
}

# bekleme süresi fonksiyonlarından gelen bekle değerini
# saat, dakika ve saniyeye çeviren fonksiyon
function kalan() {
  kalan_sure=$(printf '%02d saat'' : ''%02d dakika'' : ''%02d saniye' \
    $((bekle/3600)) $((bekle%3600/60)) $((bekle%60)))
}

# Vakit ezanları ve kullanıcı isteğine bağlı ezan dinletimi için.
function ezandinlet() {
  echo -e "${RENK7}${RENK2}\n Okuyan : ${RENK3} ${EZAN_OKUYAN} ${RENK0}"

  # Arayüzlerde mplayer çalışırken iptal düğmesine basıldığında
  # sadece bizim yönettiğimiz kopyayı sonlandır. Pid denetimi yerine
  # boru (pipe) üzerinden yönetim sağlandı.
  rm -f /tmp/mplayer.pipe{,2} 2>/dev/null
  mkfifo /tmp/mplayer.pipe
  $MPLAYER -slave -input file=/tmp/mplayer.pipe "${vakit_ezani}" 2> /dev/null

  # Ezan duası isteniyorsa
  (( ! EZAN_DUASI_OKU )) && {
    mkfifo /tmp/mplayer.pipe2
    $MPLAYER -slave -input file=/tmp/mplayer.pipe2 "${EZAN_DUASI}" 2> /dev/null
  }
  rm -f /tmp/mplayer.pipe{,2} 2>/dev/null
}

# Vakit bildirimleri için ezan dinletiminin yanında
# diğer bildirimleri de düzenleyen fonksiyon
function ezanoku() {
  clear
  echo -e "${RENK7}${RENK3} ${KONUM}${RENK2} için ${RENK3}${simdiki}${RENK2} namazı vakti${RENK5} $vakit_saati${RENK0}"

  notify-send "${KONUM} için" \
    "${simdiki} namazı vakti\n${vakit_saati}" \
    -i ${VERI_DIZINI}/veriler/ezanvakti.png -t $EZAN_BILDIRIM_SURESI"000" -h int:transient:1

  # Oynatıcı duraklatma etkin mi ezan okumadan değeri
  # denetle ve fonksiyonu çağır.
  (( ! OYNATICI_DURAKLAT )) && {
    oynatici_islem
  } || {
    ezandinlet
  }; clear
}

# Vakit ezanı okunmadan önce  anımsatma
function vakit_animsat() {
  local dakika_saniye animsatici_saniye

  # Kullanıcı anımsatma değerini sıfır olarak belirlememişse
  (( VAKIT_ANIMSAT )) && {
    dakika_saniye=$((VAKIT_ANIMSAT*60))
    animsatici_saniye=$((bekle-dakika_saniye))

    # Eğer betik vakit ezanına animsatma dakikasından
    # daha az bir süre kala çalıştırılırsa bekleme değeri
    # eksiye düşüyor.
    # Sadece değer sıfırdan büyükse anımsatma yap.
    (( animsatici_saniye > 0 )) && {
      [ ! -f /tmp/eznvktanmst_$(date +%d%m%y)_$simdiki ] && {
        :> /tmp/eznvktanmst_$(date +%d%m%y)_$simdiki
        sleep $animsatici_saniye
        notify-send "Ezanvakti ${SURUM}" "$VAKIT_ANIMSAT dakika sonra $simdiki ezanı okunacak" \
          -t $EZAN_BILDIRIM_SURESI"000" -i ${VERI_DIZINI}/veriler/ezanvakti.png -h int:transient:1
      }
    }
  }
}

# Dini günler için anımsatıcı
function gun_animsat() {
  # Betiğin mevcut oturum boyunca sadece ilk çalışmada anımsatma yapması
  # için çerez dosya denetimi ekledik. Gün değişimi için ayrıca dosyaya tarih
  # uzantısı da ekledik.
  [ ! -f /tmp/eznvkgnanmst_$(date +%d%m%y) ] && {
    # Eğer GUN_ANIMSAT değeri sıfır değilse girilen saniye değerini
    # bildirimin bekleme süresi olarak kullan.
    (( GUN_ANIMSAT )) && {
      if grep -q $(date +%d.%m.%Y) ${VERI_DIZINI}/veriler/gunler
      then
          ozel_ileti="\n\nBugün:  <b>$(grep $(date +%d.%m.%Y) ${VERI_DIZINI}/veriler/gunler | cut -d ' ' -f2-)</b>"
      elif grep -q $(date -d 'tomorrow' +%d.%m.%Y) ${VERI_DIZINI}/veriler/gunler
      then
          ozel_ileti="\n\nYarın:  <b>$(grep $(date -d 'tomorrow' +%d.%m.%Y) ${VERI_DIZINI}/veriler/gunler | cut -d ' ' -f2-)</b>"
      else
          ozel_ileti=''
      fi

      # eğer ozel_ileti boş (null) dönmemişse bildirimi gönder.
      [[ -n ${ozel_ileti} ]] && {
        notify-send "Ezanvakti $SURUM" "${ozel_ileti}" -t $GUN_ANIMSAT"000" \
          -i ${VERI_DIZINI}/veriler/ezanvakti.png -h int:transient:1
        :> /tmp/eznvkgnanmst_$(date +%d%m%y)
      }
    }
  }
}

# --ramazan için özel bildirim veren ezan okutma fonksiyonu.
function ramazan_ezanoku() {
  clear
  echo -e "${RENK7}${RENK3} ${KONUM}${RENK2} için ${RENK3}${simdiki}${RENK2} vakti${RENK5} $vakit_saati${RENK0}"

  notify-send "${KONUM} için" "${simdiki} vakti \n$vakit_saati" \
    -i ${VERI_DIZINI}/veriler/ezanvakti.png -t $EZAN_BILDIRIM_SURESI"000" -h int:transient:1 

  (( ! OYNATICI_DURAKLAT )) && {
    oynatici_islem
  } || {
    ezandinlet
  }; clear
}

# Vakitlerde ezan okunmasının ve bildirimin yönetildiği özyinelemeli ana fonksiyon.
function ezanlar() {
  denetle; bugun

  [ $SAAT -le $sabah ] && {
    simdiki=Sabah
    vakit_ezani="${SABAH_EZANI}"
    vakit_saati=$sabah_n

    # Eğer betik sabah vakti dakikası içerisinde
    # çalıştırılırsa saniyeyi önemseme, ezan okut.
    [ $SAAT -eq $sabah ] && { ezanoku; ezanlar; }
    printf "${RENK7}${RENK4}Sabah ezanı için bekleniyor...${RENK0}\n"

    bekleme_suresi $sabah_n; vakit_animsat &
    sleep $bekle; ezanoku; ezanlar
  } ||

  [ $SAAT -le $ogle ] && {
    simdiki=Öğle
    vakit_ezani="${OGLE_EZANI}"
    vakit_saati=$ogle_n

    [ $SAAT -eq $ogle ] && { ezanoku; ezanlar; }
    printf "${RENK7}${RENK4}Öğle ezanı için bekleniyor...${RENK0}\n"

    bekleme_suresi $ogle_n; vakit_animsat &
    sleep $bekle; ezanoku; ezanlar
  } ||

  [ $SAAT -le $ikindi ] && {
    simdiki=İkindi
    vakit_ezani="${IKINDI_EZANI}"
    vakit_saati=$ikindi_n

    [ $SAAT -eq $ikindi ] && { ezanoku; ezanlar; }
    printf "${RENK7}${RENK4}İkindi ezanı için bekleniyor...${RENK0}\n"

    bekleme_suresi $ikindi_n; vakit_animsat &
    sleep $bekle; ezanoku; ezanlar
  } ||

  [ $SAAT -le $aksam ] && {
    simdiki=Akşam
    vakit_ezani="${AKSAM_EZANI}"
    vakit_saati=$aksam_n

    [ $SAAT -eq $aksam ] && { ezanoku; ezanlar; }
    printf "${RENK7}${RENK4}Akşam ezanı için bekleniyor...${RENK0}\n"

    bekleme_suresi $aksam_n; vakit_animsat &
    sleep $bekle; ezanoku; ezanlar
  } ||

  [ $SAAT -le $yatsi ] && {
    simdiki=Yatsı
    vakit_ezani="${YATSI_EZANI}"
    vakit_saati=$yatsi_n

    [ $SAAT -eq $yatsi ] && { ezanoku; ezanlar; }
    printf "${RENK7}${RENK4}Yatsı ezanı için bekleniyor...${RENK0}\n"

    bekleme_suresi $yatsi_n; vakit_animsat &
    sleep $bekle; ezanoku; ezanlar
  } ||

  [ $SAAT -le 2359 ] && {
    printf "${RENK7}${RENK4}Yeni gün için bekleniyor...${RENK0}\n"

    bekleme_suresi "23:59"; sleep $((bekle+60))
    gun_animsat & ezanlar
  }
}

function kuran_dinle() {
  local dinletilecek_sure okuyan kaynak

  if [[ -n $(tr -d 0-9 <<<$girdi) ]]
  then
      printf '%b\n%b\n' \
        "ezanvakti: hatalı girdi: \`$girdi' " \
        'Sure kodu olarak 1-114 arası sayısal bir değer giriniz.'
      exit 1
  elif (( ! ${#girdi} ))
  then
      printf "ezanvakti: Bu özelliğin kullanımı için ek olarak sure kodu girmelisiniz.\n"
      exit 1
  elif (( ${#girdi} > 3 ))
  then
      printf '%b\n%b\n' \
        "ezanvakti: hatalı girdi: \`$girdi' " \
        'Girilen sure kodunun basamak sayısı <= 3 olmalı.'
      exit 1
  elif (( girdi < 1 || girdi > 114 ))
  then
      printf '%b\n%b\n' \
        "ezanvakti: hatalı girdi: \`$girdi' " \
        'Girilen sure kodu 1 <= sure_kodu <= 114 arasında olmalı.'
      exit 1
  else  # Girilen sure koduna göre değişkenin önüne sıfır ekle.
      if (( ${#girdi} == 1 ))
      then
          sure=00$girdi
      elif (( ${#girdi} == 2 ))
      then
          sure=0$girdi
      else
          sure=$girdi
      fi
  fi

  clear
  printf '%b%b\n\n' \
    "${RENK7}${RENK3}" \
    "$(grep -w $sure ${VERI_DIZINI}/veriler/sureler | gawk '{print $2}')${RENK2} suresi dinletiliyor..."

  # Öncelikle kullanıcının girdiği dizinde dosya
  # var mı denetle. Yoksa çevrimiçi dinletime yönel.
  [[ -f "${YEREL_SURE_DIZINI}/$sure.mp3" ]] && {
    dinletilecek_sure="${YEREL_SURE_DIZINI}/$sure.mp3"
    kaynak='Yerel Dizin'
    okuyan='Yerel Okuyucu'
  } || {
          # Seçilen okuyucu koduna göre okuyucunun tam adını yeni değere ata.
          [ ${OKUYAN} = AlGhamdi ] &&  okuyan="Saad el Ghamdi" \
          || {
          [ ${OKUYAN} = AsShatree ] && okuyan="As Shatry"
          } || {
          [ ${OKUYAN} = AlAjmy ] && okuyan="Ahmad el Ajmy"
          }

    dinletilecek_sure="http://www.listen2quran.com/listen/${OKUYAN}/$sure.mp3"
    kaynak='http://www.quranlisten.com'
  }

  printf '%b\n%b\n%b\n%b\n' \
    "${RENK7}${RENK2}" \
    "Okuyan : ${RENK3} ${okuyan}${RENK2}" \
    "Kaynak : ${RENK3} ${kaynak}${RENK0}"

  rm -f /tmp/mplayer.pipe{,2} 2>/dev/null
  mkfifo /tmp/mplayer.pipe
  $MPLAYER -slave -input file=/tmp/mplayer.pipe "${dinletilecek_sure}" 2> /dev/null; clear
  rm -f /tmp/mplayer.pipe{,2} 2>/dev/null
}

# Ramazan ezan bildiriminin yönetildiği ana özyinelemeli fonksiyon.
function iftarimsak() {
  denetle; bugun

  # bugun fonksiyonuna gerek yok. Sadece sabah ve akşam vakitlerini al.
  export $(gawk 'BEGIN{tarih2 = strftime("%d.%m.%Y")} {if($1 ~ tarih2) \
    {printf "sabah=%s%s\naksam=%s%s", $2,$3,$10,$11}}' "${EZANVERI}")

  [ $SAAT -le $sabah ] && {
    simdiki=İmsak
    vakit_ezani="${SABAH_EZANI}"
    vakit_saati=$sabah_n

    [ $SAAT -eq $sabah ] && { ramazan_ezanoku; iftarimsak; }
    printf "${RENK7}${RENK4}İmsak vakti için bekleniyor...${RENK0}\n"

    bekleme_suresi $sabah_n; vakit_animsat &
    sleep $bekle; ramazan_ezanoku; iftarimsak
  } ||

  [ $SAAT -le $aksam ] && {
    simdiki=İftar
    vakit_ezani="${AKSAM_EZANI}"
    vakit_saati=$aksam_n

    [ $SAAT -eq $aksam ] && { ramazan_ezanoku; iftarimsak; }
    printf "${RENK7}${RENK4}İftar vakti için bekleniyor...${RENK0}\n"

    bekleme_suresi $aksam_n; vakit_animsat &
    sleep $bekle; ramazan_ezanoku; iftarimsak
  } ||

  # Yeni güne ait hesaplamayı yapmak için saat 00:00 olana kadar
  # bekleme uyarısı ver.
  [ $SAAT -le 2359 ] && {
    printf "${RENK7}${RENK4}Yeni gün için bekleniyor...${RENK0}\n"

    bekleme_suresi "23:59"; sleep $((bekle+60))
    gun_animsat & iftarimsak
  }
}

# Hadis, bilgi ve esma için 1 ile verilen sayı arasında
# rastgele bir sayı seç. Seçilen sayı için içerik_al ile gelen
# dosya üzerinde işlem yapıp içeriği değişkene ata.
function secim_yap() {
  secilen=$((RANDOM%$1))
  (( secilen == 0 )) && secilen=$1
  alinan_yanit="$(sed -n "/#$secilen<#/,/#>$secilen#/p" ${icerik_al} | sed '1d;$d')"
}

case $1 in
  --dinle)
    case $2 in
      -s|--sabah)
        printf "${RENK7}${RENK2}Sabah${RENK3} ezanı dinletiliyor...\n"
        vakit_ezani="${SABAH_EZANI}"
        ezandinlet ;;
      -o|--[oö][gğ]le)
        printf "${RENK7}${RENK2}Öğle${RENK3} ezanı dinletiliyor...\n"
        vakit_ezani="${OGLE_EZANI}"
        ezandinlet ;;
      -i|--ikindi)
        printf "${RENK7}${RENK2}İkindi${RENK3} ezanı dinletiliyor...\n"
        vakit_ezani="${IKINDI_EZANI}"
        ezandinlet ;;
      -a|--ak[sş]am)
        printf "${RENK7}${RENK2}Akşam${RENK3} ezanı dinletiliyor...\n"
        vakit_ezani="${AKSAM_EZANI}"
        ezandinlet ;;
      -y|--yats[ıi])
        printf "${RENK7}${RENK2}Yatsı${RENK3} ezanı dinletiliyor...\n"
        vakit_ezani="${YATSI_EZANI}"
        ezandinlet ;;
      *)
        hatali_kullanim $2 ;;
    esac
  ;;
  -v|--vakitler)
    denetle; bugun

    # --osd/bildirim için renkleri sıfırla
    [[ $2 = --osd ]] || [[ $2 = --bildirim ]] && {
      RENK1=''; RENK2=''
    }

   [ $SAAT -lt $sabah ] && {
      bekleme_suresi $sabah_n; kalan
      sabah_kalan="${RENK2} $kalan_sure"
    } || {
      sabah_kalan="${RENK1} OKUNDU"
    }

    [ $SAAT -lt $ogle ] && {
      bekleme_suresi $ogle_n; kalan
      ogle_kalan="${RENK2} $kalan_sure"
    } || {
      ogle_kalan="${RENK1} OKUNDU"
    }

    [ $SAAT -lt $ikindi ] && {
      bekleme_suresi $ikindi_n; kalan
      ikindi_kalan="${RENK2} $kalan_sure"
    } || {
      ikindi_kalan="${RENK1} OKUNDU"
    }

    [ $SAAT -lt $aksam ] && {
      bekleme_suresi $aksam_n; kalan
      aksam_kalan="${RENK2} $kalan_sure"
    } || {
      aksam_kalan="${RENK1} OKUNDU"
    }

    [ $SAAT -lt $yatsi ] && {
      bekleme_suresi $yatsi_n; kalan
      yatsi_kalan="${RENK2} $kalan_sure"
    } || {
      yatsi_kalan="${RENK1} OKUNDU"
    }

    # Betiğe ikinci bir değişken verilmemişse
    (( ${#2} == 0 )) && {
      printf '%b%b%b%b%b%b\n' \
        "${RENK7}${RENK3}\n${KONUM}${RENK5} için ezan vakitleri (${TARIH} $(date +%H:%M:%S))\n\n" \
        "${RENK2}Sabah ezanı  ${RENK3}: $sabah_n   $sabah_kalan\n" \
        "${RENK2}Öğle ezanı   ${RENK3}: $ogle_n   $ogle_kalan\n" \
        "${RENK2}İkindi ezanı ${RENK3}: $ikindi_n   $ikindi_kalan\n" \
        "${RENK2}Akşam ezanı  ${RENK3}: $aksam_n   $aksam_kalan\n" \
        "${RENK2}Yatsı ezanı  ${RENK3}: $yatsi_n   $yatsi_kalan${RENK0}\n"
    } || {
      case $2 in
        -s|--sabah)
          printf "${RENK7}${RENK2}\nSabah ezanı  ${RENK3}: $sabah_n $sabah_kalan${RENK0}\n\n" ;;
        -o|--[oö][gğ]le)
          printf "${RENK7}${RENK2}\nÖğle ezanı   ${RENK3}: $ogle_n $ogle_kalan${RENK0}\n\n" ;;
        -i|--ikindi)
          printf "${RENK7}${RENK2}\nİkindi ezanı ${RENK3}: $ikindi_n $ikindi_kalan${RENK0}\n\n" ;;
        -a|--ak[sş]am)
          printf "${RENK7}${RENK2}\nAkşam ezanı  ${RENK3}: $aksam_n $aksam_kalan${RENK0}\n\n" ;;
        -y|--yats[ıi])
          printf "${RENK7}${RENK2}\nYatsı ezanı  ${RENK3}: $yatsi_n $yatsi_kalan${RENK0}\n\n" ;;
        30|--ayl[ıi]k)
          printf "\n${RENK7}${RENK6}Tarih        Sabah   Güneş   Öğle    İkindi  Akşam   Yatsı${RENK0}\n"
          gawk -v renk0=${RENK0} -v renk2=${RENK2} -v renk3=${RENK3} -v renk7=${RENK7} \
            '/^[0-9][0-9]\.[0-9]*\.[0-9]*/ \
            {printf "%s%s%s%s   0%s:%s   0%s:%s   %s:%s   %s:%s   %s:%s   %s:%s%s\n"\
            , renk7,renk3,$1,renk2,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,renk0}' "${EZANVERI}" ;;
        7|--haftal[ıi]k)
          hafta=$(date -d 'next week' +%d.%m.%Y)
          if ! grep -qo $hafta "${EZANVERI}"
          then
               printf "${RENK3}7 günlük veri bulunmuyor..${RENK0}\n"
               exit 1
          fi

          printf "\n${RENK7}${RENK6}Tarih        Sabah   Güneş   Öğle    İkindi  Akşam   Yatsı${RENK0}\n"

          grep '^[0-9][0-9]\.[0-9]*\.[0-9]*' "${EZANVERI}" | grep -B7 $hafta | gawk \
            -v renk0=${RENK0} -v renk2=${RENK2} -v renk3=${RENK3} -v renk7=${RENK7} \
            '{printf "%s%s%s%s   0%s:%s   0%s:%s   %s:%s   %s:%s   %s:%s   %s:%s%s\n"\
            , renk7,renk3,$1,renk2,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,renk0}' ;;
        --osd|--bildirim)
          notify-send "Ezanvakti ${SURUM} - vakitler" \
            "$(printf '%s\n%s\n%s\n%s\n%s' \
            "Sabah   $sabah_n   $sabah_kalan" \
            "Öğle    $ogle_n    $ogle_kalan" \
            "İkindi   $ikindi_n    $ikindi_kalan" \
            "Akşam  $aksam_n    $aksam_kalan" \
            "Yatsı     $yatsi_n    $yatsi_kalan" | sed 's:saat:sa:;s:dakika:dk:;s:saniye:sn:')" \
            -t $BILGI_BILDIRIM_SURESI"000" -h int:transient:1 ;;
        *)
          hatali_kullanim $2 ;;
      esac
    } ;;
  --arayuzlericin)
    # Arayüzlerin hızlı çalışması için vakitleri almada ayrı değişken kullanıldı.
    # Tek başına kullanılmaz.
    printf "${VAKIT_BICIMI}" 'Sabah' "$sabah_n" 'Güneş' \
      "$gunes_n" 'Öğle' "$ogle_n" 'İkindi' "$ikindi_n" 'Akşam' \
      "$aksam_n" 'Yatsı' "$yatsi_n" ;;
  --kuran)
    case $2 in
      -s|--se[cç]im)
        girdi=$3
        kuran_dinle ;;
      -h|--hatim)
        for ((i=1; i<=114; i++))
        {
          girdi=$i
          kuran_dinle; sleep 1
        } ;;
      -r|--rastgele)
        girdi=$((RANDOM%114))
        (( ! girdi )) && girdi=114
        kuran_dinle ;;
      -g|--g[uü]nl[uü]k)
        for i in $SURELER
        do
          girdi=$i
          kuran_dinle; sleep 1
        done ;;
      *)
        hatali_kullanim ${2:-null} ;;
    esac ;;
  --sureler)
    clear
    gawk -v renk0=${RENK0} -v renk2=${RENK2} -v renk3=${RENK3} -v renk7=${RENK7} \
      '{printf "%s%s%s\t%s%s%s%s\n", renk7,renk3,$1,renk2,$2,renk3,$3,renk0}' \
      ${VERI_DIZINI}/veriler/sureler | less -R ;;
  --ayet)
    [ -f "${KULLANICI_TEFSIR_DIZINI}/${TEFSIR_SAHIBI}" ] && {
      TEFSIR="${KULLANICI_TEFSIR_DIZINI}/${TEFSIR_SAHIBI}"
    } || {
    [ -f "${VERI_DIZINI}/tefsirler/${TEFSIR_SAHIBI}" ] && \
      TEFSIR="${VERI_DIZINI}/tefsirler/${TEFSIR_SAHIBI}"
    } || {
    printf "${RENK3}${TEFSIR_SAHIBI} dosyası bulunamadı.${RENK0}\n"
    exit 1
    }

    satir=$((RANDOM%6236))
    (( ! satir )) && satir=6236

    case $2 in
      -u|-t|--u[cç]birim|--terminal)
        printf '%b%b%b\n' \
          "${RENK3}\nGünlük Ayet ${RENK2}(${RENK8}" \
          "$(sed -n "$satir p" ${VERI_DIZINI}/veriler/sureler_ayetler) $satir/6236${RENK2})${RENK8}\n\n" \
          "$(sed -n "$satir p" "${TEFSIR}")${RENK0}"
        exit 0 ;;
      ?*)
        hatali_kullanim $2 ;;
    esac

    notify-send "Günlük Ayet  ($(sed -n "$satir p" ${VERI_DIZINI}/veriler/sureler_ayetler))" \
      "$(sed -n "$satir p" "${TEFSIR}")" -t $AYET_BILDIRIM_SURESI"000" -h int:transient:1 
    exit 0 ;;
  --ramazan)
    iftarimsak ;;
  --conky)
    denetle; bugun
    printf "${CONKY_BICIMI}" 'Sabah' "$sabah_n" 'Güneş' \
      "$gunes_n" 'Öğle' "$ogle_n" 'İkindi' "$ikindi_n" 'Akşam' \
      "$aksam_n" 'Yatsı' "$yatsi_n" ;;
  --iftar)
    denetle; bugun

    # Eğer şu anki saat aksam değerinden küçükse
    [ $SAAT -lt $aksam ] && {
      bekleme_suresi $aksam_n; kalan

      printf '%b\n%b\n\n' \
        "${RENK7}${RENK2}\nİftar saati : ${RENK3}$aksam_n" \
        "${RENK7}${RENK2}Kalan süre  : ${RENK3}$kalan_sure${RENK0}"
    } || {
      # Akşam değeri şu anki saatten büyük ya da eşitse
      [ $SAAT -ge $aksam ] && {
        export $(gawk '{printf "aksam_n=%s:%s", $10,$11}' \
          <(grep $(date -d 'tomorrow' +%d.%m.%Y) "${EZANVERI}"))
        bekleme_suresi_yarin $aksam_n; kalan

        printf '%b\n%b\n\n' \
          "${RENK7}${RENK2}\nİftar saati : ${RENK3}$aksam_n${RENK5} (Yarın)" \
          "${RENK2}Kalan süre  : ${RENK3}$kalan_sure${RENK0}"
      }
    };;
  --conky-iftar)
    denetle; bugun

    [ $SAAT -lt $aksam ] && {
      bekleme_suresi $aksam_n; kalan

      echo -e "İftar : $aksam_n\nKalan : $kalan_sure" |\
        sed 's:saat:sa:;s:dakika:dk:;s:saniye:sn:'
    } || {

      [ $SAAT -ge $aksam ] && {
        export $(gawk '{printf "aksam_n=%s:%s", $10,$11}' \
          <(grep $(date -d 'tomorrow' +%d.%m.%Y) "${EZANVERI}"))
        bekleme_suresi_yarin $aksam_n; kalan

        echo -e "İftar : $aksam_n (Yarın)\nKalan : $kalan_sure" |\
          sed 's:saat:sa:;s:dakika:dk:;s:saniye:sn:'
      }
    } ;;
  --hadis)
    icerik_al=${VERI_DIZINI}/veriler/kirk-hadis
    secim_yap 40

    case $2 in
      -u|-t|--u[cç]birim|--terminal)
        (( ${RENK:-RENK_KULLAN} )) && {
          echo "${alinan_yanit}" | tr -d '\\' | sed -r -e 's:\[([0-9]{1,2}(;[0-9]{1,2})?)?[m]::g' -e 's:033::g'
          exit 0
         } || {
          echo -e "${alinan_yanit}${RENK0}"
          exit 0
         } ;;
      ?*)
        hatali_kullanim $2 ;;
    esac

    notify-send "$secilen. hadis" "$(echo  "${alinan_yanit}" | sed '1d')" \
      -t $HADIS_BILDIRIM_SURESI"000" -h int:transient:1
    exit 0 ;;
  --bilgi)
    icerik_al=${VERI_DIZINI}/veriler/bilgiler
    secim_yap 157

    case $2 in
      -u|-t|--u[cç]birim|--terminal)
        (( ${RENK:-RENK_KULLAN} )) && {
          echo  "\*${alinan_yanit}" | tr -d '\\' | sed -r -e 's:\[([0-9]{1,2}(;[0-9]{1,2})?)?[m]::g' -e 's:033::g'
          exit 0
        } || {
          echo -e "${alinan_yanit}${RENK0}"
          exit 0
        } ;;
      ?*)
        hatali_kullanim $2 ;;
    esac

    notify-send "Bunları biliyor musunuz?" "$(echo  "\*${alinan_yanit}" | tr -d '\\' | sed \
      -r -e 's:\[([0-9]{1,2}(;[0-9]{1,2})?)?[m]::g' -e 's:033::g')" \
      -t $BILGI_BILDIRIM_SURESI"000" -h int:transient:1
    exit 0 ;;
  --esma)
    icerik_al=${VERI_DIZINI}/veriler/esma
    secim_yap 99

        (( ${RENK:-RENK_KULLAN} )) && {
          echo "${alinan_yanit}" | tr -d '\\' | sed -r -e 's:\[([0-9]{1,2}(;[0-9]{1,2})?)?[m]::g' -e 's:033::g'
          exit 0
        } || {
          echo -e "${alinan_yanit}${RENK0}"
          exit 0
        } ;;
  --g[uü]ncelle)
    guncelleme_yap ;;
  --g[uü]nler)
    echo -e "${RENK7}${RENK3}\n$(date +%Y)${RENK2} yılı için dini günler ve geceler\n"
    sed  -n "s:\([0-9].*$(date +%Y)\)\(.*\):$(echo -e "${RENK7}${RENK3}\1\t${RENK2}\2${RENK0}"):p" <${VERI_DIZINI}/veriler/gunler
    ;;
  --gui|--aray[uü]z)
    ezanvakti_xc="$(type -p ezanvakti)"
    # arayuz fonksiyonunu başlat.
    . ${VERI_DIZINI}/bilesenler/arayuz
    exit 0 ;;
  --gui2|--aray[uü]z2)
    ezanvakti_xc="$(type -p ezanvakti)"
    . ${VERI_DIZINI}/bilesenler/arayuz2 ;;
  -V|--s[uü]r[uü]m|--version)
    printf '%s\n\n%s\n\n%s\n%s\n%s\n%s\n' \
      "Ezanvakti $SURUM derleme: $DERLEME"\
      'Copyright (c) 2010-2012 Fatih Bostancı'\
      'Bu uygulama bir özgür yazılımdır: yeniden dağıtabilirsiniz ve/veya'\
      'Özgür Yazılım Vakfı (FSF) tarafından yayımlanan (GPL)  Genel  kamu'\
      'lisansı sürüm 3  veya daha yeni bir sürümünde belirtilen  şartlara'\
      'uymak kaydıyla, üzerinde değişiklik yapabilirsiniz.' ;;
  -h|--help|--yard[iı]m)
    betik_kullanimi ;;
  ?*)
    hatali_kullanim $1 ;;
  '')
    # Eğer betik herhangi bir değişken verilmeden çalıştırılmışsa
    # ezanlar fonksiyonunu çağır.
    ezanlar ;;
esac
# vim:set ts=2 sw=2 et:
